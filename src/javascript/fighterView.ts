import { Fighter } from './models/models';
import View from './helpers/view';
import { FighterEventHandler } from './fightersView';

class FighterView extends View {
  public createFighter(fighter: Fighter, 
      handleClick: FighterEventHandler, 
      selectFighter: FighterEventHandler
    ): HTMLElement {
    const { name, source } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    const checkboxElement = this.createCheckbox();
    const fighterContainer = this.createElement({ tagName: 'div', className: 'fighter' });
    
    fighterContainer.append(imageElement, nameElement, checkboxElement);
  
    const preventCheckboxClick = (ev: Event): void => ev.stopPropagation();
    const onCheckboxClick = async (ev: Event): Promise<void> => await selectFighter(ev, fighter);
    const onFighterClick = async (ev: Event): Promise<void> => await handleClick(ev, fighter);
  
    fighterContainer.addEventListener('click', onFighterClick);
    checkboxElement.addEventListener('change', onCheckboxClick);
    checkboxElement.addEventListener('click', preventCheckboxClick);
  
    return fighterContainer;
  }
  
  private createName(name: string): HTMLElement {
    const nameElement = this.createElement({ tagName: 'span', className: 'name' });
    nameElement.innerText = name;
  
    return nameElement;
  }
  
  private createImage(source: string): HTMLElement {
    const attributes = { src: source };
    const imgElement = this.createElement({ tagName: 'img', className: 'fighter-image', attributes });
  
    return imgElement;
  }
  
  private createCheckbox(): HTMLElement {
    const label = this.createElement({ tagName: 'label', className: 'custom-checkbox' });
    const span = this.createElement({ tagName: 'span', className: 'checkmark' });
    const attributes = { type: 'checkbox' };
    const checkboxElement = this.createElement({ tagName: 'input', attributes });
  
    label.append(checkboxElement, span);

    return label;
  }
}

export default FighterView;