import { apiHelper } from '../helpers/apiHelper';
import { Fighter, FighterInfo } from '../models/models';

class FighterService {
  async getFighters(): Promise<Fighter[]> {
    const endpoint = 'fighters.json';
    const apiResult = await apiHelper.callApi(endpoint, 'GET');

    return apiResult;
  }

  async getFighterDetails(id: string): Promise<FighterInfo> {
    try {
      const endpoint = `details/fighter/${id}.json`;
      const apiResult = await apiHelper.callApi(endpoint, 'GET');

      return JSON.parse(atob(apiResult.content));
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
