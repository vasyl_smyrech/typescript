import { ElementData } from "../models/elementData";

class View {
  createElement(elementData: ElementData<keyof HTMLElementTagNameMap>):  HTMLElement {
    const element: HTMLElement = document.createElement(elementData.tagName);
    
    if (elementData.className)
      element.classList.add(elementData.className);
  
    if (elementData.attributes) {
      Object.keys(elementData.attributes).forEach(key => 
        element.setAttribute(key, elementData.attributes![key]));
    }

    return element;
  }
}

export default View;