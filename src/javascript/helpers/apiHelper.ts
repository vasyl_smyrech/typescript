import { fightersDetails, fighters } from './mockData';
import { Fighter } from '../models/fighter';
import { FighterInfo } from '../models/fighterDetails';
import { API_URL } from '../constants/apiURL';

class ApiHelper{
  private useMockAPI = true;

  async callApi(endpoint: string, method: 'GET' | 'POST' | 'PUT' | 'DELETE') {
    const url = API_URL + endpoint;
    const options = {
      method,
    };

    return this.useMockAPI
      ? this.fakeCallApi(endpoint)
      : fetch(url, options)
          .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
          .then((result) => JSON.parse(atob(result.content)))
          .catch((error) => {
            throw error;
          });
}

async fakeCallApi(endpoint: string) : Promise<Fighter[] | FighterInfo>{
  const response = endpoint === 'fighters.json' ? fighters : this.getFighterById(endpoint);

  return new Promise((resolve, reject) => {
    setTimeout(() => (
      response 
      ? resolve(response) 
      : reject(Error('Failed to load'))), 500);
  });
}

async getFighterById(endpoint: string) : Promise<FighterInfo> {
  const start = endpoint.lastIndexOf('/');
  const end = endpoint.lastIndexOf('.json');
  const id = endpoint.substring(start + 1, end);

  return fightersDetails.find((it) => it._id === (id ? id : endpoint))!;}
}

export const apiHelper = new ApiHelper();