import FighterView from './fighterView';
import FighterInfoModal from './modals/fighterInfoModal';
import Fight from './fight';
import WinnerModal from './modals/winnerModal';
import { Fighter, FighterInfo } from './models/models';
import { apiHelper } from './helpers/apiHelper';
import View from './helpers/view';

class FightersView extends View {
  private fightersDetailsCache = new Map<string, FighterInfo>();
  private fighterView: FighterView;

  constructor(
    private fighters: Fighter[]
  ) 
  { 
    super();
    this.fighterView = new FighterView();
  }

  createFighters(): HTMLElement {
    const selectFighterForBattle = this.createFightersSelector();
    const showFighterDetails = this.showFighterDetails();
    const fighterElements = this.fighters.map((fighter: Fighter) => 
      this.fighterView.createFighter(fighter, showFighterDetails, selectFighterForBattle));
    const fightersContainer = this.createElement({ tagName: 'div', className: 'fighters' });
  
    fightersContainer.append(...fighterElements);
  
    return fightersContainer;
  }

  createFightersSelector(): FighterEventHandler {
    const selectedFighters = new Map<string, FighterInfo>();
    const checkedCheckboxes = new Map<string, HTMLInputElement>();

    return async (event: Event, fighter: Fighter): Promise<void> => {
  
      const fullInfo = await this.getFighterInfo(fighter._id);
      const checkbox = <HTMLInputElement>event.target;
  
      if (checkbox.checked) {
        selectedFighters.set(fighter._id, fullInfo);
        checkedCheckboxes.set(fighter._id, checkbox);
      } else { 
        selectedFighters.delete(fighter._id);
        checkedCheckboxes.delete(fighter._id);
      }
  
      if (selectedFighters.size === 2) {
        const winner = new Fight()
          .fight(
            Array.from(selectedFighters.values())[0], 
            Array.from(selectedFighters.values())[1]
            );
  
        new WinnerModal(() => { 
          setTimeout(
            () => checkedCheckboxes.forEach(c => c.checked = false), 500
          );
        }).showWinnerModal(winner);

        selectedFighters.clear();
      }
    }
  }
  
  showFighterDetails(): FighterEventHandler  {
    return async (event: Event, fighter: Fighter): Promise<void> => {
      const fullInfo = await this.getFighterInfo(fighter._id);
      new FighterInfoModal(fullInfo);
    }
  }  

  async getFighterInfo(fighterId: string): Promise<FighterInfo> {
    let fighterInfo: FighterInfo;

    if (this.fightersDetailsCache.has(fighterId)) {
      fighterInfo = this.fightersDetailsCache.get(fighterId)!;
    } else {
      fighterInfo = await apiHelper.getFighterById(fighterId);
      this.fightersDetailsCache.set(fighterId, fighterInfo);
    }

    return {
      _id: fighterInfo._id, 
      name: fighterInfo.name, 
      source: fighterInfo.source,
      attack: fighterInfo.attack,
      defense: fighterInfo.defense,
      health: fighterInfo.health
    }
  }
}

export type FighterEventHandler =  (event: Event, fighter: Fighter) => Promise<void>;

export default FightersView;

