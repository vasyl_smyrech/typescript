import { FighterInfo } from "./models/models";

class Fight {

  public fight(firstFighter: FighterInfo, secondFighter: FighterInfo): FighterInfo {
    let isFirstFighterAttacker = true;

    while(firstFighter.health > 0 && secondFighter.health > 0){
      const currentAttacker = isFirstFighterAttacker ? firstFighter : secondFighter;
      const currentEnemy = isFirstFighterAttacker ? secondFighter : firstFighter;
      isFirstFighterAttacker = !isFirstFighterAttacker;
      currentEnemy.health -= this.getDamage(currentAttacker, currentEnemy);
    }

    return firstFighter.health > 0 ? firstFighter : secondFighter;
  }
  
  private getDamage(attacker: FighterInfo, enemy: FighterInfo): number{
    const damage = this.getHitPower(attacker) - this.getBlockPower(enemy);
    return damage <=  0 ? 0 : damage;
  }
  
  private getHitPower(fighter: FighterInfo): number {
    return fighter.attack * this.getChance();
  }
  
  private getBlockPower(fighter: FighterInfo): number{
    return fighter.defense * this.getChance();
  } 

  private getChance(): number {
    const min = 1;
    const max = 2;
    return Math.random() * (max - min) + min;
  }
}

export default Fight;