import { fighterService } from './services/fightersService'
import FightersView from './fightersView';

class App {
  private rootElement: HTMLElement = document.getElementById('root')!;
  private loadingElement: HTMLElement = document.getElementById('loading-overlay')!;

  fightersElement: HTMLElement | undefined;

  constructor() {
    this.startApp();
  }

  public async startApp(): Promise<void> {
    try {
      this.loadingElement.style.visibility = 'visible';

      const fighters = await fighterService.getFighters();
      const fightersView = new FightersView(fighters);

      this.fightersElement = fightersView.createFighters();
      this.rootElement.appendChild(this.fightersElement!);
    } catch (error) {
      console.warn(error);
      
      this.rootElement.innerText = 'Failed to load data';
    } finally {
      this.loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;