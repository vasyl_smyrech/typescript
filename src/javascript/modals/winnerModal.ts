import Modal, { ModalEventHandler } from './modal';
import { FighterInfo } from '../models/models';

class WinnerModal extends Modal {

  constructor(closedHandler?: ModalEventHandler) {
    super(closedHandler);
  }

  public showWinnerModal(fighter: FighterInfo) {
    const title = `${fighter.name} is the winner!!!`;
    const bodyElement = this.createFighterImage(fighter);
    this.showModal(title, bodyElement);
  }
  
  createFighterImage(fighter: FighterInfo): HTMLElement {
    const fighterDetails = this.createElement({ tagName: 'div', className: 'modal-body' });
    const remainHealth = this.createElement({tagName: 'p'});
    remainHealth.innerHTML = `Remain health: ${fighter.health}`;
    const attributes = { src: fighter.source };
    const imgElement = this.createElement({ tagName: 'img', attributes });
  
    fighterDetails.append(remainHealth, imgElement);
    return fighterDetails;
  }
}

export default WinnerModal
