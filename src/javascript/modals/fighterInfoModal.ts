import Modal from './modal';
import { FighterInfo } from '../models/models';

class FighterInfoModal extends Modal {

  constructor(fighter: FighterInfo) {
    super();
    this.showFighterDetailsModal(fighter);
  }

  private showFighterDetailsModal(fighter: FighterInfo): void {
    const title = 'Fighter info';
    const bodyElement = this.createFighterDetails(fighter);
    this.showModal(title, bodyElement);
  }
  
  createFighterDetails(fighter: FighterInfo): HTMLElement {
    const { name } = fighter;
  
    const fighterDetails = this.createElement({ tagName: 'div', className: 'modal-body' });
    const nameElement = this.createElement({ tagName: 'span', className: 'fighter-name' });
    const attackElement = this.createElement({ tagName: 'p', className: 'fighter-attack' });
    const defenseElement = this.createElement({ tagName: 'p', className: 'fighter-defense' });
    const healthElement = this.createElement({ tagName: 'p', className: 'fighter-health' });
   
    nameElement.innerText = name;
    attackElement.innerText = `Attack: ${fighter.attack}`;
    defenseElement.innerText = `Defense: ${fighter.defense}`;
    healthElement.innerText = `Health: ${fighter.health}`;
 
    fighterDetails.append(nameElement, attackElement,  defenseElement, healthElement);
  
    return fighterDetails;
  }
}

export default FighterInfoModal
