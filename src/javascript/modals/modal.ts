import View from '../helpers/view';

class Modal extends View {
  constructor(private closedHandler?: ModalEventHandler){
    super();
  }
  
  protected showModal(title: string, bodyElement: HTMLElement) {
    const root = this.getModalContainer();
    const modal = this.createModal(title, bodyElement); 
    
    root.append(modal);
  }
  
  private getModalContainer(): HTMLElement {
    return document.getElementById('root')!;
  }
  
  private createModal(title: string, bodyElement: HTMLElement): HTMLElement {
    const layer = this.createElement({ tagName: 'div', className: 'modal-layer' });
    const modalContainer = this.createElement({ tagName: 'div', className: 'modal-root' });
    const header = this.createHeader(title);
  
    modalContainer.append(header, bodyElement);
    layer.append(modalContainer);
  
    return layer;
  }
  
  private createHeader(title: string): HTMLElement {
    const headerElement = this.createElement({ tagName: 'div', className: 'modal-header' });
    const titleElement = this.createElement({ tagName: 'span' });
    const closeButton = this.createElement({ tagName: 'div', className: 'close-btn' });
    
    titleElement.innerText = title;
    closeButton.innerText = '×';
    closeButton.addEventListener('click', this.hideModal);
    if(this.closedHandler)
      closeButton.addEventListener('click', this.closedHandler);
    headerElement.append(title, closeButton);
    
    return headerElement;
  }
  
  private hideModal(event: Event): void {
    const modal = document.getElementsByClassName('modal-layer')[0];
    modal?.remove();
  }
}

export type ModalEventHandler = (event: Event) => void;

export default Modal;
