import { Fighter } from "./fighter";

export interface FighterInfo extends Fighter {
    health: number;
    attack: number;
    defense: number;
}