export interface ElementData <S extends keyof HTMLElementTagNameMap>
{
    tagName: S;
    className?: string, 
    attributes?: { [qualifiedName: string]: string }
};